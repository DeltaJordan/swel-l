﻿/*
    SWEL-L 'Starbound Workshop Easy Linker - Linux'
    Actually cross-platform!
    Version: 1.0.0
    Created by: DeltaJordan
    Copyright: (c) Feb 2022, All Rights Reserved.
*/

public static class Program
{
    private static readonly string defaultSteamDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), ".steam/steam/steamapps");

    private static string steamDir;
    private static string starboundDir;

    public static void Main()
    {
        Console.WriteLine();
        Console.Write($"Enter the path to your 'steamapps' directory ({defaultSteamDir}): ");
        steamDir = Console.ReadLine();
        steamDir = Path.GetFullPath(string.IsNullOrWhiteSpace(steamDir) ? defaultSteamDir : steamDir);
        Console.WriteLine(steamDir);
        Console.Write($"Enter the path to your Starbound server directory ({Path.Combine(defaultSteamDir, "common", "Starbound")}): ");
        starboundDir = Console.ReadLine();
        starboundDir = Path.GetFullPath(string.IsNullOrWhiteSpace(starboundDir) ? Path.Combine(defaultSteamDir, "common", "Starbound") : starboundDir);
        Console.WriteLine(starboundDir);

        OpenMenu();
    }

    private static void OpenMenu()
    {
        string menu = @"_____________Starbound Workshop Easy Linker ______________
______________________ Main Menu _________________________
1. Create Workshop Mod Links
2. Create a Copy of Workshop Mods
3. Change Paths
-------------------------------------
__________PRESS 'Q' TO QUIT__________";
        Console.WriteLine(menu);
        Console.Write("Please select an option (1): ");
        string selectedOption = Console.ReadLine();

        switch (selectedOption)
        {
            case "1":
            case "":
            case null:
                CreateLinks();
                break;
            case "2":
                break;
            case "3":
                Main();
                return;
            default:
                Environment.Exit(0x0);
                return;
        }
    }

    private static void CreateLinks()
    {
        Console.Clear();

        EnumerationOptions enumerationOptions = new();
        enumerationOptions.RecurseSubdirectories = true;

        try
        {
            foreach (string directory in Directory.EnumerateDirectories(Path.Combine(steamDir, "workshop/content/211820/")))
            {
                string workshopId = new DirectoryInfo(directory).Name;

                foreach (string file in Directory.EnumerateFiles(directory, "*.pak", enumerationOptions))
                {
                    string destLinkPath = Path.Combine(starboundDir, "mods", $"{workshopId}-{Path.GetFileName(file)}");

                    if (File.Exists(destLinkPath))
                    {
                        Console.WriteLine(destLinkPath + " already exists in the mods directory. Skipping...");
                        continue;
                    }

                    File.CreateSymbolicLink(destLinkPath, file);
                    Console.WriteLine($"{file} => {destLinkPath}");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Caught the following exception while attempting to create links.");
            Console.WriteLine(ex);
            OpenMenu();
            return;
        }

        Console.Beep();
        Console.WriteLine("Finished! Press any key to continue...");
        Console.ReadKey(true);
        OpenMenu();
    }

    private static void CopyContent()
    {
        Console.Clear();

        EnumerationOptions enumerationOptions = new();
        enumerationOptions.RecurseSubdirectories = true;

        try
        {
            foreach (string directory in Directory.EnumerateDirectories(Path.Combine(steamDir, "workshop/content/211820/")))
            {
                string workshopId = new DirectoryInfo(directory).Name;

                string destDir = Path.Combine(starboundDir, "mods", workshopId);
                if (Directory.Exists(destDir))
                {
                    Console.WriteLine(destDir + " already exists. Skipping...");
                }

                CopyDirectory(destDir, directory, true);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Caught the following exception while attempting to copy workshop content.");
            Console.WriteLine(ex);
            OpenMenu();
            return;
        }

        Console.Beep();
        Console.WriteLine("Finished! Press any key to continue...");
        Console.ReadKey(true);
        OpenMenu();
    }

    private static void CopyDirectory(string sourceDir, string destinationDir, bool recursive)
    {
        // Get information about the source directory
        var dir = new DirectoryInfo(sourceDir);

        // Check if the source directory exists
        if (!dir.Exists)
            throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

        // Cache directories before we start copying
        DirectoryInfo[] dirs = dir.GetDirectories();

        // Create the destination directory
        Directory.CreateDirectory(destinationDir);

        // Get the files in the source directory and copy to the destination directory
        foreach (FileInfo file in dir.GetFiles())
        {
            string targetFilePath = Path.Combine(destinationDir, file.Name);
            file.CopyTo(targetFilePath);
        }

        // If recursive and copying subdirectories, recursively call this method
        if (recursive)
        {
            foreach (DirectoryInfo subDir in dirs)
            {
                string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                CopyDirectory(subDir.FullName, newDestinationDir, true);
            }
        }
    }
}